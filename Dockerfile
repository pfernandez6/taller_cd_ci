FROM python:3.8.10-slim-buster

WORKDIR /app/project

RUN cd /app/project

COPY . .

RUN pip install -r requirements.txt

CMD python3 app.py
